function extract_video_frames(filename, output_folder, N)
    vid = VideoReader(filename);
    count = 1;
    for i = linspace(1,vid.NumFrames,N)
        frame = read(vid, round(i));
        imwrite(frame, fullfile(output_folder, sprintf('%06d.jpg', count)))
        count = count + 1;
    end
end
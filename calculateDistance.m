%% Camera Params
focal_length = [1.900950014835293e+03,1.899553900598943e+03];
error_focal_length = [2.875064272008363,2.308264315659521];
f = focal_length(1);
error_f = error_focal_length(1);

%% Target measurement
D = 7.46178; % cm
error_D = 0.02479; % cm

%% Target image measurements
target_50_px = [262, 261, 261];
target_100_px = [141, 140, 140];
target_150_px = [96, 94, 95];

d = [mean(target_50_px), mean(target_100_px), mean(target_150_px)];
dispersion_ = [dispersion(target_50_px), dispersion(target_100_px), dispersion(target_150_px)];
error_d = 1;

Z = D*f*d.^(-1)
error_Z = f*d.^(-1)*error_D + (D*f*d.^(-2)/2).*error_d + D*d.^(-1)*error_f
(D*f/2)*error_d
(D*f*d.^(-2)/2).*error_d
absolute_error_Z = abs([50,100,150]-Z)
relative_errror_Z = absolute_error_Z.*Z.^-1*100
function d = dispersion(vector)
d = (max(vector)-min(vector))/mean(vector)*100;
return 
end

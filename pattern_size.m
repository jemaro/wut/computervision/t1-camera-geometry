% Constants
in2mm = 25.4;
% Known parameters
screen_diagonal_in = 21.5;
screen_width_px = 1920;
screen_height_px = 1080;
AR = 16/9;
pattern_height_px = 181;
error_pattern_height_px = 1;
pattern_width_px = 181;
target_height_px = 301;
% Conversions
screen_height_in  = screen_diagonal_in/sqrt(1+(16/9)^2)
height_in2px = screen_height_px/screen_height_in
screen_height_mm = screen_height_in*in2mm
height_px2mm = screen_height_mm/screen_height_px
screen_width_in  = screen_height_in*16/9
screen_width_mm = screen_width_in*in2mm
width_px2mm = screen_width_mm/screen_width_px
% Application
pattern_height_mm = pattern_height_px*height_px2mm
error_pattern_height_mm = error_pattern_height_px*height_px2mm
target_height_mm = target_height_px*height_px2mm